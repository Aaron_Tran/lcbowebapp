<%@ page import ="java.util.*" %>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script>
function goBack() {
  window.history.back();
}
</script>
</head>
<body class="page-header">
<center>
<h1>
    Available Brands
</h1>
<%
List result= (List) request.getAttribute("brands");
Iterator it = result.iterator();
out.println("<br>At LCBO, We have <br><br>");
while(it.hasNext()){
out.println(it.next()+"<br>");
}
%>
<br>
<button onclick="goBack()">Go Back</button>
</body>
</html>